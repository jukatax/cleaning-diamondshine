<?php
	$file = 'includes/header.php';
	if (file_exists($file) && is_readable($file))
			{	include($file); }
?>
<!--******************************END HEADER***************************************-->
		
		<header><span itemscope itemtype="http://schema.org/LocalBusiness">
			<div class="h_wrapper">
				<nav>
					<h1><a href="index.php"><?php echo '<span itemprop="name"><strong>'.$company_name.'</strong></span> <br /> <span itemprop="telephone">'.$mob.'</span>'?></a></h1>
					<a href="index.php" id="home">Home</a>
				    <a href="services.php">Services</a>
				    <a href="prices.php">Prices</a>
					<a href="#" class="selected">Contact</a>
				</nav>

			</div>
			</span>
		</header>
<!--******************************Form***************************************-->
<div class="clear_fix"></div>
	<section class='form'>
				<h2>Contact us</h2>
				<form method='post' action='submit_form.php'>
					<div class='formentry'>
						<label for='name'>Name&#42;:</label>
						<input type='text' name='name' id='name' placeholder='Your name' required='required' autofocus size='35' />
					</div>

					<div class='formentry'>
						<label for='email'>Email&#42;:</label>
						<input type='email' name='email' id='email' placeholder='Your email' required='required' size='35'  />
					</div>

					<div class='formentry'>
						<label for='tel'>Phone number&#42;:</label>
						<input type='tel' name='tel' id='tel' placeholder='Your phone' required='required' size='35'  />
					</div>

					<div class='formentry'>
						<label for='org'>Organization:</label>
						<input type='text' name='org' id='org' placeholder='Organization' size='35'  />
					</div>


					<div class='formentry'>
					<label for='date'>Preferred date&#42;:</label>
					<input type='text' name='date' id='date' placeholder='Example 01/Mar/2013' required='required' size='35'  />
					</div>

					<div class='formentry'>
					<label for='time'>Preferred time&#42;:</label>
					<input type='text' name='time' id='time' placeholder='Example 5:30pm' required='required' size='35'   />
					</div>

					<div class='formentry'>
					<label for='comments'>Your comments/ preferences:</label>
					<textarea  placeholder='Enter your comments here' name='comments' id='comments' cols='35' rows='5'>
					</textarea>
					</div>


					<div class='clear_fix'></div>
					<input id='submit' type='submit' value='Submit Form' />
				</form>
			</section>
<script>
    function iefix(){
        var val_time=$('#time').attr('placeholder');
        var val_date=$('#date').attr('placeholder');
        $('#time').attr('value',val_time);
        $('#date').attr('value',val_date);
        alert(val_time);
    }
</script>
<script type="text/javascript">   //csroll to top script
			window.addEventListener("load", function () { 
			// Set a timeout... 
			setTimeout(function () { 
			// Hide the address bar! 
			window.scrollTo(0, 1); 
			}, 0); 
			}); 
		</script> 
<!--******************************FOOTER***************************************-->
<?php
	$file = 'includes/footer.php';
	if (file_exists($file) && is_readable($file))
			{	include($file); }
?>