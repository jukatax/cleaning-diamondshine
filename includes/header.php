<?php
    $title=basename($_SERVER['SCRIPT_NAME'],'.php');
	if($title=='index'){
		$title='Cleaning Services';
	}
	else{
		$title=ucfirst($title);
		$title=str_replace('_',' ', $title);
	}
	$tel='02089972450';
	$mob='07542084878';
	$company_name='SA Diamond Shine';
	ini_set('date.timezone', 'Europe/London');
	$thisyear=date('y');
	$startyear=10;
?>
<?php

            $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
            $iphone2 = stristr($_SERVER['HTTP_USER_AGENT'],"iPhone");
            $android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
            $android2 = stristr($_SERVER['HTTP_USER_AGENT'],"Android");
            $palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
            $palmpre2 = stristr($_SERVER['HTTP_USER_AGENT'],"webOS");
            $berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
            $berry2 = stristr($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
            $ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
            $ipod2 = stristr($_SERVER['HTTP_USER_AGENT'],"iPod");
            $sentfrom2='unknown';
            if ($iphone == true || $iphone2 == true)
                {
                    $sentfrom2='iPhone';
                //header('Location: http://www.google.com/');
                //OR    ($iphone || $android || $palmpre || $ipod || $berry == true)
                //echo "<script>window.location='http://www.google.com'</script>";
                }
            elseif ($android== true || $android2== true)
                {
                    $sentfrom2="Android";
                            }
            elseif ($berry == true || $berry2 == true)
                {
                    $sentfrom2="Blackberry";
                                }
            elseif ($ipod == true || $ipod2 == true)
                {
                    $sentfrom2="iPod";
                                }
            elseif ($palmpre== true || $palmpre2== true)
                {
                    $sentfrom2="webOS";
                                }
            else
                {
                    $sentfrom2="PC";
                                }
        ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo "{$company_name}-{$title}";?></title>
		<meta charset="utf-8">
		<?php echo "<meta name='description'content='Cleaning services in London-{$company_name}-{$title}' />";?> 
		<meta name="keywords" content="Cleaning,house cleaning,flat cleaning,office cleaning,residential cleaning,carpet and curtains cleaning,spring cleaning,one-off cleaning" />
		<meta name="viewport" content="width=device-width, maximun-scale=1.0" />
		<meta name="author" content="Yuliyan Yordanov" />
		<link rel="stylesheet" href="css/normalize.css" type="text/css" media="all">
		<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen">
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="js/scroll.js"></script>
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="js/html5.js"></script>
		   <script>
		       iefix(); //fix the lack of placeholders for forms in ie.
			  document.createElement('header');
			  document.createElement('nav');
			  document.createElement('section');
			  document.createElement('article');
			  document.createElement('aside');
			  document.createElement('footer');
			</script>
		<![endif]-->


	</head>
	<body>