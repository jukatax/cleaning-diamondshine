<?php
	$file = 'includes/header.php';
	if (file_exists($file) && is_readable($file))
			{	include($file); }
?>
<!--******************************END HEADER***************************************-->
		
		<header><span itemscope itemtype="http://schema.org/LocalBusiness">
			<div class="h_wrapper">
				<nav>
					<h1><a href="index.php"><?php echo '<span itemprop="name"><strong>'.$company_name.'</strong></span> <br /> <span itemprop="telephone">'.$mob.'</span>'?></a></h1>
					<a href="#" id="home" class="selected">Home</a>
				    <a href="services.php">Services</a>
				    <a href="prices.php">Prices</a>
					<a href="contact.php">Contact</a>
				</nav>
				<div class="h_slider">
					<img src="imgs/livingroom.jpg" width="480" alt="cleaning" />
				</div>
			</div>
			</span>
		</header>
		<section class="h_services">
			<div class="h_service">
				<h2>House Cleaning &amp; Maid Services</h2>
				<a href="services.php#house">Learn More</a>
			</div>
			<div class="h_service">
				<h2>Commercial &amp; Office Cleaning</h2>
				<a href="services.php#office">Learn More</a>
			</div>
			<div class="h_service">
				<h2>Window &amp; Carpet Cleaning</h2>
				<a href="services.php#carpet">Learn More</a>
			</div>
			<div class="clear_fix"></div>
		</section>
		<section class="h_services">
			<div class="h_service2">
				<h2>OUR SERVICES</h2>
				<a href="services.php#house">Residential Cleaning</a>
				<a href="services.php#office">Commercial Cleaning</a>
				<a href="services.php#carpet">Carpet Cleaning</a>
			</div>
			<div class="h_service3">
				<h2>ABOUT US</h2>
				<p><?php echo $company_name?> offers a variety of cleaning services to meet the diverse needs of every client.
				    All our services are performed by highly qualified cleaning staff.
				    They are experienced, friendly and responsible individuals, who are dedicated to their work.
			    </p>
			    <h3>Our company’s goals are:</h3>
			    <ul>
			        <li> To provide a high quality, reasonable cost cleaning service, meeting all our customers’ needs</li>
			        <li>To ensure our customers are always 100% satisfied with the work we carry out.</li>
			        <li> To provide a reliable, competitive service that our customers depend on again and again.</li>
			    </ul>
			</div>
			<div class="clear_fix"></div>
		</section>
<script>
		function slideshow(){
            var current=$('#photos .show');
            var next=current.next().length?current.next():current.parent().children(':first'); //to check if this is the last child and there is no next child,check next length.if 0 then next is the first child of the parent!
            current.hide().removeClass('show');
            next.fadeIn('slow').addClass('show');
            setTimeout(slideshow,4000);

        }
</script>

<script type="text/javascript">   //csroll to top script
			window.addEventListener("load", function () { 
			// Set a timeout... 
			setTimeout(function () { 
			// Hide the address bar! 
			window.scrollTo(0, 1); 
			}, 0); 
			}); 
		</script> 
<!--******************************FOOTER***************************************-->
<?php
	$file = 'includes/footer.php';
	if (file_exists($file) && is_readable($file))
			{	include($file); }
?>

