<?php
    $file = 'includes/header.php';
    if (file_exists($file) && is_readable($file))
            {   include($file); }
?>
<!--******************************END HEADER***************************************-->

		<header>		<span itemscope itemtype="http://schema.org/LocalBusiness">
			<div class="h_wrapper">
				<nav>
					<h1><a href="index.php"><?php echo '<span itemprop="name"><strong>'.$company_name.'</strong></span> <br /><span itemprop="telephone">'.$mob.'</span>'?></a></h1>
                    <a href="index.php" id="home">Home</a>
                    <a href="services.php">Services</a>
                    <a href="#" class="selected">Prices</a>
                    <a href="contact.php">Contact</a>
                </nav>

            </div>
			</span>
        </header>
<div class="clear_fix"></div>
    <div class="prices">
        <h2>Price list</h2>
        <table>
            <th>Carpet cleaning</th><th>Price</th>
<tr><td>Single Bedroom</td>   <td>&#163;20.00</td></tr>
<tr><td>Double Bedroom</td>  <td>&#163;25.00</td></tr>
<tr><td>Lounge from</td>  <td>&#163;20.00</td></tr>
<tr><td>Dining Room from</td>     <td>&#163;20.00</td></tr>
<tr><td>Stairs - price per step</td><td> &#163;1.50</td></tr>
<tr><td>Landing</td> <td>&#163;5.00</td></tr>
<tr><td>Hallway from</td>    <td> &#163;10.00</td></tr>
<tr><td>Toilet/Bathroom from</td>    <td>&#163;5.00</td></tr>
<tr><td>Rug from</td>     <td>&#163;10.00</td></tr>
<tr><td>Commercial/Office</td>   <td>&#163;1.30-&#163;1.50 per m<sup>2</sup></td></tr>
<th>Upholstery - Steam Cleaning</th><th>Price</th>
<tr><td>Chair</td>   <td>&#163;4.00</td></tr>
<tr><td>Dining Chair</td>    <td>&#163;7.00</td></tr>
<tr><td>Armchair</td>    <td>&#163;15.00</td></tr>
<tr><td>Two-Seated Sofa</td> <td>&#163;25.00</td></tr>
<tr><td>Three-Seated Sofa</td>   <td>&#163;35.00</td></tr>
<th>Curtains & Mattresses - Steam Cleaning</th><th>Price</th>
<tr><td>Half length pair of curtains from</td>  <td> &#163;20.00</td></tr>
<tr><td>Full length pair of curtains from</td>   <td>&#163;35.00</td></tr>
<tr><td>Mattress (single)</td>   <td>&#163;15.00</td></tr>
<tr><td>Mattress (double)</td>    <td>&#163;20.00</td></tr>
<tr><td>Mattress (king size) </td>  <td>&#163;25.00</td></tr>
<tr><td>Pillow</td>  <td>&#163;5.00</td></tr>
<th>Domestic Cleaning</th><th>Price</th>
<tr><td>Regular Domestic Cleaning </td>   <td>from &#163;9.00 per hour</td></tr>
<tr><td>Office Cleaning (regular)</td>    <td>from &#163;9.50 per hour</td></tr>
<tr><td>Studio Flat</td> <td>&#163;79.00</td></tr>
<tr><td>Studio Flat, including carpet cleaning</td>  <td>&#163;90.00</td></tr>
<tr><td>1 Bedroom Flat</td>  <td>&#163;115.00</td></tr>
<tr><td>1 Bedroom Flat, including carpet cleaning</td>   <td>&#163;140.00</td></tr>
<tr><td>2 Bedroom Flat / House</td>  <td>&#163;140.00</td></tr>
<tr><td>2 Bedroom Flat / House, incl. carpet cleaning</td>   <td>&#163;185.00</td></tr>
<tr><td>3 Bedroom Flat / House</td>  <td>&#163;185.00</td></tr>
<tr><td>3 Bedroom Flat / House, incl. carpet cleaning</td>   <td>&#163;235.00</td></tr>
<tr><td>4 Bedroom Flat / House</td>  <td>&#163;200.00</td></tr>
<tr><td>4 Bedroom Flat / House, incl. carpet cleaning</td>   <td>&#163;290.00</td></tr>
        </table>


    </div>



<script type="text/javascript">   //csroll to top script
			window.addEventListener("load", function () { 
			// Set a timeout... 
			setTimeout(function () { 
			// Hide the address bar! 
			window.scrollTo(0, 1); 
			}, 0); 
			}); 
		</script> 


<!--******************************FOOTER***************************************-->
<?php
    $file = 'includes/footer.php';
    if (file_exists($file) && is_readable($file))
            {   include($file); }
?>