<?php
    $file = 'includes/header.php';
    if (file_exists($file) && is_readable($file))
            {   include($file); }
?>
<!--******************************END HEADER***************************************-->

		<header id="top">		<span itemscope itemtype="http://schema.org/LocalBusiness">
			<div class="h_wrapper">
				<nav>
					<h1><a href="index.php"><?php echo '<span itemprop="name"><strong>'.$company_name.'</strong></span> <br /> <span itemprop="telephone">'.$mob.'</span>'?></a></h1>
                    <a href="index.php" id="home">Home</a>
                    <a href="#" class="selected">Services</a>
                    <a href="prices.php">Prices</a>
                    <a href="contact.php">Contact</a>
                </nav>
            </div>
			<span>
        </header>
        <div class="clear_fix"></div>
        <nav id="local_nav">
            <a href="#house">House Cleaning &amp; Maid Services <img src="imgs/arrow.png" alt="right arrow" width="14" height="14" /></a>
            <a href="#office">Commercial &amp; Office Cleaning <img src="imgs/arrow.png" alt="right arrow" width="14" height="14" /></a>
            <a href="#carpet">Window &amp; Carpet Cleaning <img src="imgs/arrow.png" alt="right arrow" width="14" height="14" /></a>
        </nav>
        <div class="clear_fix"></div>
        <div class="entry" id="house" name="house">
            <div class="service">
                <h2>House Cleaning &amp; Maid Services</h2>
                <img src="imgs/home_cleaning.png" width="590" height="200" alt="clean house"/>
                <p>We specialise in domestic cleaning and private household maintenance for people with busy routine. Nowadays is difficult to combine career with family responsibilities and keep your home in perfect condition. You can carry on your daily activities and rest assured that your home will be cleaned thoroughly and professionally by one of our professionally trained domestic cleaning teams.
We pay great attention to detail. You let us know if there are any particular areas or items in your home that need special attention (or any areas you don't want cleaned for some reason) and our cleaning staff will honor your requests.
We will prepare a cleaning service designed around the unique details of your home and your personal cleaning requirements.
You can expect a consistent and reliable cleaning service with the highest level of customer satisfaction.
Our Regular Domestic cleaning program includes everything on this list and more.
                    <ul>
                        <li>All Rooms:Vacuum/Mop all floors, carpets, rugs and stairs</li>
                        <li>Vacuum cloth furniture including under cushions</li>
                        <li>Clean glass surfaces.</li>
                        <li>Scrub toilet, including bowl, seat, lid, tank, and base.</li>
                        <li>Clean showers, baths, and sinks inside and out.</li>
                        <li>Clean stove drip pans, burner grates, and control knobs.</li>
                    </ul>
                </p>
            </div>

            <div class="totop"><a href="#top">Go to top</a></div>
        </div>

        <div class="entry" id="office" name="office">
            <div class="service">
                <h2>Commercial &amp; Office Cleaning</h2>
                <img src="imgs/office_cleaning3.png" width="590" height="200" alt="clean office"/>
                <p>Your office is the face of your company.
                    This is the place where you hold business meetings and the place where important business decisions are made.
                    It is of extreme importance that your office must also be a place where you create a healthy environment and give you a more positive atmosphere for your employees and clients.
                    Our flexible office cleaning services and deep clean service will ensure that the cleanliness of your office is maintained at all times.
                    Our <?php echo $company_name?> office cleaners are experienced and reliable.
                    We know that your working hours are reserved for business, so we won't interrupt your busy schedule.
                    We will provide the office cleaning sessions in time specified by you.
                    Our Office cleaning service includes:
                    <ul>
                        <li>Dust and clean furniture window ledges, window sill, tops of pictures, radiators, doors, door frames, skirting boards, light switches.</li>
                        <li>Phone and keyboard sterilizing.</li>
                        <li>Dust and clean desks and tables.</li>
                        <li>Vacuum clean all soft furniture and all carpet areas.</li>
                        <li>Hoover the floor and wash it if necessary</li>
                        <li>Toilets , bathroom and kitchen areas - clean mirrors and glass, wipe tiles , clean hob, wipe outside cupboards</li>
                    </ul>
                </p>
            </div>

            <div class="totop"><a href="#top">Go to top</a></div>
        </div>

        <div class="entry" id="carpet" name="carpet">
            <div class="service">
                <h2>Window &amp; Carpet Cleaning</h2>
                <img src="imgs/carpet_cleaning.png" width="590" height="200" alt="clean carpet_window"/>
                <p>
                	When it comes to carpet cleaning in London, the results mainly depend on the cleaning techniques that are used. 
                	At <?php echo $company_name?> we use the latest materials and cleaning equipment as well as techniques to make our services affordable as well as efficient. 
                	Our professional London cleaners are very experienced and well trained to work with any type of carpet. 
                	All of our staff members are courteous as well as customer satisfaction oriented. 
                	We can clean all types of fabrics and carpets and the materials that we use for cleaning and environmental friendly as well as safe. We can also deal with any type of stains and can protect the carpets against damage. 
                	Regular carpet cleaning can easily prolong the life of a carpet.
                	Our Carper steam cleaning technicians use the hot water extraction method to remove dirt, grime, and allergens from your carpets. 
                	The hot water extraction method is the preferred method of carpet cleaning amongst carpet manufacturers which recommend a professional cleaning every 
                	12 to 18 months to maintain the appearance and warranties of your carpeting. 
                	We advise even twice or more a year if you have small children, pets or if you suffer from allergies.
                	Our Carpet cleaning service includes:
                </p>
                
                    <ul>
                        <li>Pre-examine areas for carpet problems.</li>
                        <li>We move most furniture that one man can move. We DO NOT move antiques, plants, electronics or breakables. We will move furniture a few feet, clean the carpet, and return the furniture to the original spot.</li>
                        <li>Pre-treatment of spots at the technician's discretion.</li>
                        <li>Clean carpet with hot water extraction process. This method will inject hot water with a mild cleaning agent (designed to enhance the suspension of soil); then immediately extract it using the powerful vacuum of the machine.</li>
                        <li>Depending on the type of carpet, the final step is raking. This lifts the pile and helps the carpet dry faster.</li>
                    </ul>
                
            </div>
            <div class="totop"><a href="#top">Go to top</a></div>
        </div>
<div class="clear_fix"></div>
		<div class="entry">
			<div class="book">
				<h2>How to book</h2>
				<p><img src="imgs/LOGO.png" width="100" height="100" alt="SA Diamond Shine logo"/>
					<span class="text">
						You can book any of the following cleaning services from <?php echo $company_name?> by calling us on <?php echo $mob?>. 
						We have a dedicated team of staff on hand to help with any questions that you may have about our services. 
						You'll be able to schedule an appointment over the phone or with our easy to use <a href="contact.php">Booking form</a>.
					</span>
				</p>
			</div>
			<div class="clear_fix"></div>
		<div class="totop"><a href="#top">Go to top</a></div>
        </div>



<div class="clear_fix"></div>
<!--******************************FOOTER***************************************-->
<?php
    $file = 'includes/footer.php';
    if (file_exists($file) && is_readable($file))
            {   include($file); }
?>