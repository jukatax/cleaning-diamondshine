<?php
	$file = 'includes/header.php';
	if (file_exists($file) && is_readable($file))
			{	include($file); }
?>
<!--******************************END HEADER***************************************-->

		<header>		<span itemscope itemtype="http://schema.org/LocalBusiness">
			<div class="h_wrapper">
				<nav>
					<h1><a href="index.php"><?php echo '<span itemprop="name"><strong>'.$company_name.'</strong></span> <br /> <span itemprop="telephone">'.$mob.'</span>'?></a></h1>
					<a href="index.php" id="home">Home</a>
				    <a href="services.php">Services</a>
				    <a href="prices.php">Prices</a>
					<a href="#" class="selected">Contact</a>
				</nav>
			</div>
			<span>
		</header>
		<div class="clear_fix"></div>
<!--******************************Form***************************************-->
<?php
	function printform(){
		echo "<section class='form'>
				<h2>Contact us</h2>
				<form method='post' action='submit_form.php'>
					<div class='formentry'>
						<label for='name'>Name&#42;:</label>
						<input type='text' name='name' id='name' placeholder='Your name' required='required' autofocus size='35' />
					</div>

					<div class='formentry'>
						<label for='email'>Email&#42;:</label>
						<input type='email' name='email' id='email' placeholder='Your email' required='required' size='35'  />
					</div>

					<div class='formentry'>
						<label for='tel'>Phone number&#42;:</label>
						<input type='tel' name='tel' id='tel' placeholder='Your phone' required='required' size='35'  />
					</div>

					<div class='formentry'>
						<label for='org'>Organization:</label>
						<input type='text' name='org' id='org' placeholder='Organization' size='35'  />
					</div>


					<div class='formentry'>
					<label for='date'>Preferred date&#42;:</label>
					<input type='text' name='date' id='date' placeholder='Example 01/Mar/2013' required='required' size='35'  />
					</div>

					<div class='formentry'>
					<label for='time'>Preferred time&#42;:</label>
					<input type='text' name='time' id='time' placeholder='Example 5:30pm' required='required' size='35'   />
					</div>

					<div class='formentry'>
					<label for='comments'>Your comments/ preferences:</label>
					<textarea  placeholder='Enter your comments here' name='comments' id='comments' cols='35' rows='5'>
					</textarea>
					</div>


					<div class='clear_fix'></div>
					<input id='submit' type='submit' value='Submit Form' />
				</form>
			</section>" ;
	}

	function spamcheck($field)
				  {
				  //filter_var() sanitizes the e-mail
				  //address using FILTER_SANITIZE_EMAIL
				  $field=filter_var($field, FILTER_SANITIZE_EMAIL);

				  //filter_var() validates the e-mail
				  //address using FILTER_VALIDATE_EMAIL
				  if(filter_var($field, FILTER_VALIDATE_EMAIL))
				    {
				    return TRUE;
				    }
				  else
				    {
				    return FALSE;
				    }
				  }

	if (isset($_POST['email']))
					  {//if "email" is filled out, proceed

					  //check if the email address is invalid
					  $mailcheck = spamcheck($_REQUEST['email']);
						  if ($mailcheck==FALSE)
						    {
						    echo "<span class='annotation'>Invalid email address!</span>";
						    printform();
						    }
						  elseif($_POST["tel"]==" "){
						  	echo "<span class='annotation'>Invalid Mobile number!Please fill the form correctly!</span>";
						  	printform();
						  }
						  elseif($_POST["name"]==" "){
						  	echo "<span class='annotation'>You need to provide your name!</span>";
						  	printform();
						  }
						  elseif($_POST["date"]==" "){
						  	echo "<span class='annotation'>You need to provide a date!</span>";
						  	printform();
						  }
						  elseif($_POST["time"]==" "){
						  	echo "<span class='annotation'>You need to provide a time!</span>";
						  	printform();
						  }
									  else{//send email
									  	$cc="ahmedova.sisi@gmail.com";
										$to="sadiamondshine@gmail.com";
										$subject="Urgent - New contact from web!";
										$name=$_POST["name"];
										$tel=$_POST["tel"];
										$email=$_POST["email"];
										$org=$_POST["org"];
										$date=$_POST["date"];
										$time=$_POST["time"];
										$comments=$_POST["comments"];
										$ourmail="sadiamondshine@gmail.com";
										$message="<h2>Dear all,</h2>"."\n ".
			"<h3>This is a new request from the website form</h3>" ."\n ".
			"<p>From: " .$name."</p>\n ".
			"<p>Email: ".$email. "</p>\n".
			"<p>Tel: ".$tel. "</p>\n".
			"<p>Organization: ".$org. "</p>\n".
			"<p>For the date: " .$date. "</p>\n".
			"<p>At: " .$time. " o'clock" ."</p>\n".
			"<p>Comments: ".$comments."</p>\n".
			"<p>Sent from: " .$sentfrom2."</p>";
			//Sednd a confirmation email to the customer ======================================================================
			$message2="<h2>Dear ".$name.",</h2>" ."\n "."<p>Thank you for contacting us.</p>
			<p>Here is a summary of your request:</p>
			<p>Your name(s): " .$name."  ".$surname."</p>" ."\n "."<p>Your contact Email: ".$email. "</p>" ."\n "."<p>Your Organizatioin: ".$org. "</p>" ."\n "."<p>Date: " .$date. "</p>" ."\n "."<p>At: " .$time. "o'clock". "</p>" ."\n "."<p>Comments: ".$comments;
										// Always set content-type when sending HTML email
										$headers = "MIME-Version: 1.0" . "\r\n";
										$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";

										// More headers
										$headers .= 'From: ' .$email. "\r\n";
										$headers .= 'Cc: ' . $cc. "\r\n";
										//send email to us
										mail($to,$subject,$message,$headers);
										//send email back to the customer with his submitted data
										mail($email,'SA Diamond Shine-Do Not Reply-Thank You for contacting us' ,$message2,"MIME-Version: 1.0" . "\r\n"."Content-type:text/html;charset=iso-8859-1" . "\r\n"." From: ".$ourmail);
										//print success message on the page
										echo"<section class='submition_results'>
													<h3>Message sent</h3>
													<p>Thank You, <span class='annotation'>".$name."</span> for contacting us!Here is a summary of your request:</p>
														<ul>
															<li>Date:<span class='annotation'>".$date."</span></li>
															<li>Time:<span class='annotation'>".$time."</span> o'clock</li>
															<li>Comments:<span class='annotation'>".$comments."</span></li>
														</ul>
													<p>We will confirm with you shortly!</p>

												</section>";
										printform();

										}//end else
					  	}//end if
				else{//if "email" is NOT filled out, print form

				printform();
				}

?>
<script>
	var name=<?php echo $name;?>;
	var email=<?php echo $email;?>;
	var tel=<?php echo $tel;?>;
	if($("#name").val()!="" ||$("#email").val()!="" || $("#tel").val()!="" ){
		$("#submit").click(function(){
			$("#name").val(name);
			$("#email").val(email);
			$("#tel").val(tel);
		});
	}
</script>
<script type="text/javascript">   //csroll to top script
			window.addEventListener("load", function () { 
			// Set a timeout... 
			setTimeout(function () { 
			// Hide the address bar! 
			window.scrollTo(0, 1); 
			}, 0); 
			}); 
		</script> 
<!--******************************FOOTER***************************************-->
<?php
	$file = 'includes/footer.php';
	if (file_exists($file) && is_readable($file))
			{	include($file); }
?>